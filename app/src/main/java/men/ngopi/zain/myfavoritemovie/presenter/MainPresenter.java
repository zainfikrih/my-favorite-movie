package men.ngopi.zain.myfavoritemovie.presenter;

import android.content.Context;
import android.util.Log;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import men.ngopi.zain.myfavoritemovie.database.Database;
import men.ngopi.zain.myfavoritemovie.model.Movie;
import men.ngopi.zain.myfavoritemovie.view.MainView;

public class MainPresenter {
    private MainView view;
    private Context context;
    private static MainPresenter mainPresenter;

    private List<Movie> movies;
    private CompositeDisposable disposables;

    public static MainPresenter getInstance() {
        return mainPresenter;
    }

    public MainPresenter(MainView view, Context context, CompositeDisposable compositeDisposable) {
        mainPresenter = this;
        this.view = view;
        this.context = context;
        this.disposables = compositeDisposable;
    }

    public void onSuccessGetMovies(List<Movie> movies) {
        view.onSuccessGetMovies(movies);
    }

    public void getMoviesObs() {
        Database.getInstance(context).getMoviesObs(context)
                .subscribeOn(Schedulers.io())
                .delay(1000, TimeUnit.MILLISECONDS)
                .repeat()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Movie>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Movie> movies) {
                        view.onSuccessGetMovies(movies);
                        Log.i("OnNExt", String.valueOf(movies.size()));
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("onError", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.e("onComplete", "complete");
                    }
                });
    }
}
