package men.ngopi.zain.myfavoritemovie.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

public class Animate {
    /*
     * Animate
     * */

    // Animate Show
    public static void show(final View v, final int visibility) {
        v.animate()
                .translationY(0)
                .setDuration(400)
                .alpha(1.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.setVisibility(visibility);
                    }
                });
    }

    // Animate Hide
    public static void hide(final View v, final int visibility) {
        v.animate()
                .translationY(-v.getHeight())
                .setDuration(400)
                .alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        v.setVisibility(visibility);
                    }
                });
    }

}
