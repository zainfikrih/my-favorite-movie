package men.ngopi.zain.myfavoritemovie.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.card.MaterialCardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.myfavoritemovie.DetailActivity;
import men.ngopi.zain.myfavoritemovie.R;
import men.ngopi.zain.myfavoritemovie.model.Movie;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private Context context;
    private List<Movie> movies;
    private LayoutInflater layoutInflater;

    public MovieAdapter(Context context) {
        this.context = context;
        movies = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    public void setMovies(List<Movie> movies) {
        this.movies.clear();
        this.movies = movies;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.item_movie, viewGroup, false);
        return new MovieAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        // Setup url
        String urlBackdrop = "https://image.tmdb.org/t/p/w500" + movies.get(i).getBackdrop_path();
        String urlPoster = "https://image.tmdb.org/t/p/w500" + movies.get(i).getPoster_path();

        if (movies.get(i).getBackdrop_path() != null) {
            // Set image backdrop
            Glide.with(context)
                    .load(urlBackdrop)
                    .apply(new RequestOptions().override(500, 200))
                    .centerCrop()
                    .timeout(3000)
                    .into(viewHolder.ivBackdrop);
        }

        if (movies.get(i).getPoster_path() != null) {
            // Set image psoter
            Glide.with(context)
                    .load(urlPoster)
                    .centerCrop()
                    .timeout(3000)
                    .into(viewHolder.ivPoster);
        }


        try {
            // Set movie name
            if (movies.get(i).getTitle() == null) {
                viewHolder.tvTitle.setText(movies.get(i).getOriginal_name());
            } else {
                viewHolder.tvTitle.setText(movies.get(i).getTitle());
            }

            // Set movie year
            if (movies.get(i).getRelease_date() == null) {
                String date = movies.get(i).getFirst_air_date();
                String year = date.substring(0, 4);
                viewHolder.tvYear.setText(year);
            } else {
                String date = movies.get(i).getRelease_date();
                String year = date.substring(0, 4);
                viewHolder.tvYear.setText(year);
            }
        } catch (Exception e) {

        }

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = viewHolder.getAdapterPosition();
                Intent detailsMovieIntent = new Intent(context, DetailActivity.class);
                detailsMovieIntent.putExtra(DetailActivity.EXTRA_DETAILS_MOVIE, movies.get(position));
                context.startActivity(detailsMovieIntent);
            }
        });
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_backdrop_item_movie)
        ImageView ivBackdrop;
        @BindView(R.id.img_poster_item_movie)
        ImageView ivPoster;
        @BindView(R.id.tv_title_item_movie)
        TextView tvTitle;
        @BindView(R.id.tv_year_item_movie)
        TextView tvYear;
        @BindView(R.id.movie_card)
        MaterialCardView cardView;

        ViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
