package men.ngopi.zain.myfavoritemovie.view;

import java.util.List;

import men.ngopi.zain.myfavoritemovie.model.Movie;

public interface MainView {
    void onSuccessGetMovies(List<Movie> movies);
}
