package men.ngopi.zain.myfavoritemovie.presenter;

import android.content.Context;

import java.util.List;

import men.ngopi.zain.myfavoritemovie.database.Database;
import men.ngopi.zain.myfavoritemovie.model.Movie;
import men.ngopi.zain.myfavoritemovie.view.DetailView;
import men.ngopi.zain.myfavoritemovie.view.MainView;

public class DetailPresenter {
    private DetailView view;
    private Context context;
    private static DetailPresenter detailPresenter;

    public static DetailPresenter getInstance() {
        return detailPresenter;
    }

    public DetailPresenter(DetailView view, Context context) {
        detailPresenter = this;
        this.view = view;
        this.context = context;
    }

    public void setView(Movie movie){
        view.showMovie(movie);
    }
}
